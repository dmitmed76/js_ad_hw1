class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    get name() {
      return this._name;
    }
  
    set name(value) {
      this._name = value;
    }
  
    get age() {
      return this._age;
    }
  
    set age(value) {
      this._age = value;
    }
  
    get salary() {
      return this._salary;
    }
  
    set salary(value) {
      this._salary = value;
    }
  }

  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }
  
    get salary() {
      return this._salary * 3;
    }
  
    set salary(value) {
      this._salary = value;
    }
  }
  
const programmer1 = new Programmer("John", 25, 5000, ["JavaScript", "Java"]);
const programmer2 = new Programmer("Alice", 30, 6000, ["Python", "C++"]);

console.log(programmer1);
console.log(programmer2);

  